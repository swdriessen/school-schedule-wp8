﻿using System.ComponentModel;
using System.IO.IsolatedStorage;

namespace SchoolSchedule.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private readonly IsolatedStorageSettings _settings;

        public ViewModelBase()
        {
            _settings = IsolatedStorageSettings.ApplicationSettings;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public IsolatedStorageSettings Settings
        {
            get { return _settings; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}