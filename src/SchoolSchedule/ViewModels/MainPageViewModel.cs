﻿using SchoolSchedule.Data;
using SchoolSchedule.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace SchoolSchedule.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private readonly DataService _dataService;
        private ObservableCollection<string> _classes;
        private ObservableCollection<Student> _students;
        private string _selectedClass;
        private Student _selectedStudent;
        private bool _isLoading;
        private string _statusMessage;
        private bool _canSelectClasses;
        private bool _canSelectStudents;
        private bool _canGetSchedule;

        public MainPageViewModel()
        {
            _dataService = new DataService();
            _classes = new ObservableCollection<string>();
            _students = new ObservableCollection<Student>();

            if (DesignerProperties.IsInDesignTool)
                CreateDesignData();
        }

        public ObservableCollection<string> Classes
        {
            get { return _classes; }
            set
            {
                _classes = value;
                OnPropertyChanged("Classes");
            }
        }
        public ObservableCollection<Student> Students
        {
            get { return _students; }
            set
            {
                _students = value;
                OnPropertyChanged("Students");
            }
        }
        public string SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                if (_selectedClass == value)
                    return;

                _selectedClass = value;
                OnPropertyChanged("SelectedClass");
                LoadStudents();
            }
        }
        public Student SelectedStudent
        {
            get { return _selectedStudent; }
            set
            {
                if (_selectedStudent == value)
                    return;

                _selectedStudent = value;
                OnPropertyChanged("SelectedStudent");
            }
        }
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                _statusMessage = value;
                OnPropertyChanged("StatusMessage");
            }
        }

        public bool CanSelectClasses
        {
            get { return _canSelectClasses; }
            set
            {
                _canSelectClasses = value;
                OnPropertyChanged("CanSelectClasses");
            }
        }
        public bool CanSelectStudents
        {
            get { return _canSelectStudents; }
            set
            {
                _canSelectStudents = value;
                OnPropertyChanged("CanSelectStudents");
            }
        }
        public bool CanGetSchedule
        {
            get { return _canGetSchedule; }
            set
            {
                _canGetSchedule = value;
                OnPropertyChanged("CanGetSchedule");
            }
        }
        
        public async void LoadClasses()
        {
            try
            {
                CanSelectClasses = false;
                StatusMessage = "Getting Classes...";
                IsLoading = true;

                var classes = await _dataService.DownloadClassesAsync();

                IsLoading = false;

                Classes = new ObservableCollection<string>(classes);
                SelectedClass = classes.First();

                CanSelectClasses = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Loading Classes", MessageBoxButton.OK);
                IsLoading = false;
            }
        }
        private async void LoadStudents()
        {
            try
            {
                if (string.IsNullOrEmpty(SelectedClass))
                    return;

                CanSelectStudents = false;
                CanGetSchedule = false;

                StatusMessage = "Getting Students...";
                IsLoading = true;

                var students = await _dataService.DownloadStudentsAsync(SelectedClass);

                Students = new ObservableCollection<Student>(students.OrderBy(s => s.Name));
                SelectedStudent = Students.First();

                IsLoading = false;
                CanSelectStudents = true;
                CanGetSchedule = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Loading Students", MessageBoxButton.OK);
                IsLoading = false;
            }
        }






        #region Design Data
        private void CreateDesignData()
        {
            _classes.Add("design class");
            _students.Add(new Student("design student"));

            _canSelectClasses = true;
            _canSelectStudents = true;
            _canGetSchedule = true;
        }
        #endregion
    }
}
