﻿using System;
using System.ComponentModel;
using System.Windows;
using SchoolSchedule.Data;
using SchoolSchedule.Models;

namespace SchoolSchedule.ViewModels
{
    public class SchedulePageViewModel : ViewModelBase
    {
        private readonly DataService _dataService;
        private Schedule _schedule;
        private string _classname;
        private string _studentId;
        private string _studentName;
        private bool _isLoading;
        private string _statusMessage;

        public SchedulePageViewModel()
        {
            _dataService = new DataService();
            _schedule = new Schedule();

            if (DesignerProperties.IsInDesignTool)
                CreateDesignData();
        }

        public Schedule Schedule
        {
            get { return _schedule; }
            set
            {
                _schedule = value;
                OnPropertyChanged("Schedule");
            }
        }
        public string StudentName
        {
            get { return _studentName; }
            set
            {
                _studentName = value;
                OnPropertyChanged("StudentName");
            }
        }
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }
        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                _statusMessage = value;
                OnPropertyChanged("StatusMessage");
            }
        }
        public void LoadSchedule(string classname, string studentId)
        {
            _classname = classname;
            _studentId = studentId;

            ReloadSchedule();
        }
        public async void ReloadSchedule()
        {
            try
            {
                StatusMessage = string.Format("Getting Schedule...");

                IsLoading = true;

                Schedule = await _dataService.DownloadScheduleAsync(_classname, _studentId);

                IsLoading = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Loading Schedule", MessageBoxButton.OK);
            }
        }

        #region Design Data
        private void CreateDesignData()
        {
            _studentName = "Design Student";

            _schedule = new Schedule();
            _schedule.Monday.Add(new Subject(1, "NME1", "ab", "01", "1", false));
            _schedule.Monday.Add(new Subject(2, "", "vrij", "", "", true));
            _schedule.Monday.Add(new Subject(3, "NME3", "ab", "03", "3", false));
            _schedule.Monday.Add(new Subject(4, "NME4", "ab", "04", "4", false));
            _schedule.Monday.Add(new Subject(5, "NME5", "ab", "05", "5", false));
            _schedule.Monday.Add(new Subject(6, "NME6", "ab", "06", "6", false));
            _schedule.Monday.Add(new Subject(7, "NME7", "ab", "07", "7", false));
            _schedule.Monday.Add(new Subject(8, "", "vrij", "", "", true));
            _schedule.Monday.Add(new Subject(9, "NME9", "ab", "09", "9", false));
        }
        #endregion
    }
}