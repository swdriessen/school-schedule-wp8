﻿namespace SchoolSchedule.Models
{
    public class Subject
    {
        public Subject()
        {
        }
        public Subject(int hour, string name, string teacher, string room, string groupId, bool isCancelled)
        {
            Hour = hour;
            Name = name;
            Teacher = teacher;
            Room = room;
            GroupId = groupId;
            Cancelled = isCancelled;
        }

        public int Hour { get; set; }
        public string Name { get; set; }
        public string Teacher { get; set; }
        public string Room { get; set; }
        public string GroupId { get; set; }
        public bool Cancelled { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1} {2} {3} {4}", Hour, Teacher, Room, Name, GroupId);
        }
    }
}