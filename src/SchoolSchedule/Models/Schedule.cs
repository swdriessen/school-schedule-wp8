﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace SchoolSchedule.Models
{
    public class Schedule
    {
        public Schedule()
        {
            Monday = new ObservableCollection<Subject>();
            Tuesday = new ObservableCollection<Subject>();
            Wednesday = new ObservableCollection<Subject>();
            Thursday = new ObservableCollection<Subject>();
            Friday = new ObservableCollection<Subject>();
        }

        public ObservableCollection<Subject> Monday { get; set; }
        public ObservableCollection<Subject> Tuesday { get; set; }
        public ObservableCollection<Subject> Wednesday { get; set; }
        public ObservableCollection<Subject> Thursday { get; set; }
        public ObservableCollection<Subject> Friday { get; set; }


        //debug methods
        public void PrintCollection(string title, IEnumerable<Subject> collection)
        {
            Debug.WriteLine("===== {0} =====", title);
            foreach (var subject in collection)
            {
                Debug.WriteLine("\t{0}", subject);
            }
            Debug.WriteLine("");
        }
        public void PrintSchedule()
        {
            PrintCollection("Monday", Monday);
            PrintCollection("Tuesday", Tuesday);
            PrintCollection("Wednesday", Wednesday);
            PrintCollection("Thursday", Thursday);
            PrintCollection("Friday", Friday);
        }
    }
}