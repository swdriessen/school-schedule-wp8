﻿namespace SchoolSchedule.Models
{
    public class Student
    {
        public Student()
        {
        }

        public Student(string name)
        {
            Name = name;
        }

        public string StudentId { get; set; }
        public string PreNameId { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
