﻿using System;

namespace SchoolSchedule.Models
{
    public class ClassSpan
    {
        public TimeSpan Start { get; set; }
        public TimeSpan Finish { get; set; }

        public ClassSpan(int startHour, int startMinute, int finishHour, int finishMinute)
            : this(new TimeSpan(startHour, startMinute, 0), new TimeSpan(finishHour, finishMinute, 0))
        {

        }

        public ClassSpan(TimeSpan start, TimeSpan finish)
        {
            Start = start;
            Finish = finish;
        }

        public bool IsNowActive()
        {
            var now = DateTime.Now.TimeOfDay;
            return now >= Start && now < Finish;
        }
    }
}