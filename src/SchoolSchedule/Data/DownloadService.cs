﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace SchoolSchedule.Data
{
    public abstract class DownloadService
    {
        protected async Task<string> DownloadPageSourceAsync(string url)
        {
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            string pageSource;
            using (var responseStream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(responseStream))
                {
                    pageSource = await reader.ReadToEndAsync();
                }
            }

            return pageSource;
        }
    }
}
