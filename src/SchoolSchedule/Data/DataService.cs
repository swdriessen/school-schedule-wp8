﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using SchoolSchedule.Models;

namespace SchoolSchedule.Data
{
    public class DataService : DownloadService
    {
        private const string BaseUrl = "http://roosters.gepro-osi.nl/roosters/rooster.php";

        public async Task<List<string>> DownloadClassesAsync()
        {
            var url = string.Format("{0}?wijzigingen=1&type=Leerlingrooster&school=620", BaseUrl);

            Debug.WriteLine("DownloadClassesAsync");
            Debug.WriteLine("\turl:{0}", url);

            var source = await DownloadPageSourceAsync(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            var classes = new List<string>();

            var optionNodes = doc.DocumentNode.SelectNodes("//select[@name=\"afdeling\"]/option"); //<option>4gym</option> this is how the combobox content it looks in html source
            foreach (var optionNode in optionNodes)
            {
                var className = optionNode.NextSibling.InnerText.Trim();

                classes.Add(className);
            }

            Debug.WriteLine("Parsed {0} Classes", classes.Count);

            return classes;
        }

        public async Task<IEnumerable<Student>> DownloadStudentsAsync(string className)
        {
            var url = string.Format("{0}?afdeling={1}&wijzigingen=1&type=Leerlingrooster&school=620", BaseUrl, className);

            Debug.WriteLine("DownloadStudentsAsync");
            Debug.WriteLine("\tclass:{0}", className);
            Debug.WriteLine("\turl:{0}", url);

            var source = await DownloadPageSourceAsync(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            var students = new List<Student>();

            var optionNodes = doc.DocumentNode.SelectNodes("//select[@name=\"leerling\"]/option");
            foreach (var optionNode in optionNodes)
            {
                var student = new Student();

                student.StudentId = optionNode.GetAttributeValue("value", "unknownId");

                var nameParts = optionNode.NextSibling.InnerText.Split('-');

                student.PreNameId = nameParts[0].Trim();
                student.Name = nameParts[1].Trim();
                student.Class = className;

                students.Add(student);
            }

            Debug.WriteLine("Parsed {0} Students", students.Count);
            return students;
        }

        public async Task<Schedule> DownloadScheduleAsync(string className, string studentId)
        {
            var url = string.Format("{0}?afdeling={1}&leerling={2}&wijzigingen=1&type=Leerlingrooster&school=620", BaseUrl, className, studentId);

            Debug.WriteLine("DownloadScheduleAsync");
            Debug.WriteLine("\tclass:{0}", className);
            Debug.WriteLine("\tstudentId:{0}", studentId);
            Debug.WriteLine("\turl:{0}", url);

            var source = await DownloadPageSourceAsync(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            var contentNodes = doc.DocumentNode.SelectNodes("//div/table/tr[2]/td/table[4]/tr/td[3]/table//table");

            var schedule = new Schedule();

            const int days = 5;
            const int hours = 9;
            const int offset = days;

            for (int dayIndex = 0; dayIndex < days; dayIndex++)
            {
                for (int hourIndex = 0; hourIndex < hours; hourIndex++)
                {
                    var nodeIndex = dayIndex + (hourIndex * offset);
                    var subjectCollection = contentNodes[nodeIndex].SelectNodes(".//tr//td");

                    var subject = new Subject
                    {
                        Teacher = subjectCollection[0].InnerText.Trim(),
                        Room = subjectCollection[2].InnerText.Trim(),
                        Name = subjectCollection[4].InnerText.Trim(),
                        GroupId = subjectCollection[5].InnerText.Trim(),
                        Hour = hourIndex + 1
                    };

                    if (subject.ToString().Contains("vrij")) //cancelled property to make a color foreground
                        subject.Cancelled = true;

                    if (dayIndex == 0)
                        schedule.Monday.Add(subject);
                    else if (dayIndex == 1)
                        schedule.Tuesday.Add(subject);
                    else if (dayIndex == 2)
                        schedule.Wednesday.Add(subject);
                    else if (dayIndex == 3)
                        schedule.Thursday.Add(subject);
                    else if (dayIndex == 4)
                        schedule.Friday.Add(subject);

                    //TODO: Refactor this, maybe add indexer on schedule class, create custom collection class, debug print methods
                }
            }

            schedule.PrintSchedule();

            return schedule;
        }
    }
}