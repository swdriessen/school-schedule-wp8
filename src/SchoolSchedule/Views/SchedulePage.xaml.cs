﻿using Microsoft.Phone.Controls;
using SchoolSchedule.ViewModels;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace SchoolSchedule.Views
{
    public partial class SchedulePage : PhoneApplicationPage
    {
        private readonly SchedulePageViewModel _viewModel;

        public SchedulePage()
        {
            InitializeComponent();

            //DataContext set in xaml
            _viewModel = (SchedulePageViewModel)DataContext;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString == null)
            {
                MessageBox.Show("There was no query string passed. Unable to load schedule.", "Navigation Error", MessageBoxButton.OK);
                //refactor to user friendly message, then navigate back
                //NavigationService.GoBack();
            }
            else
            {
                var query = NavigationContext.QueryString;
                if (query.ContainsKey("class") && query.ContainsKey("studentId") && query.ContainsKey("studentName"))
                {
                    _viewModel.StudentName = query["studentName"];

                    var classname = query["class"];
                    var studentId = query["studentId"];

                    _viewModel.LoadSchedule(classname, studentId);

                    try
                    {
                        schedulePivot.SelectedIndex = ((int)DateTime.Today.DayOfWeek) - 1; //0 = sunday
                    }
                    catch (Exception)
                    {
                        schedulePivot.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("Not all required data was passed through. Unable to load schedule.", "Navigation Error", MessageBoxButton.OK);
                }
            }
        }

        private void RefreshClick(object sender, EventArgs e)
        {
            _viewModel.ReloadSchedule();
        }
    }
}