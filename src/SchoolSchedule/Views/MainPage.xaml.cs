﻿using System.Diagnostics;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using SchoolSchedule.ViewModels;
using System;
using System.Windows;

namespace SchoolSchedule.Views
{
    public partial class MainPage : PhoneApplicationPage
    {
        private readonly MainPageViewModel _viewModel;

        public MainPage()
        {
            InitializeComponent();

            //DataContext set in xaml
            _viewModel = (MainPageViewModel)DataContext;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {
                if (_viewModel.Settings.Contains("class") && _viewModel.Settings.Contains("studentId") && _viewModel.Settings.Contains("studentName"))
                {
                    GetSchedule(true);
                }
                else
                {
                    _viewModel.LoadClasses();
                }
            }
            if (e.NavigationMode == NavigationMode.Back)
            {
                if (!_viewModel.CanGetSchedule)
                {
                    _viewModel.LoadClasses();
                }
            }
        }

        private void GetSchedule(bool loadFromSettings)
        {
            string query;

            if (loadFromSettings)
            {
                query = string.Format("?class={0}&studentId={1}&studentName={2}",
                    _viewModel.Settings["class"],
                    _viewModel.Settings["studentId"],
                    _viewModel.Settings["studentName"]);
            }
            else
            {
                //save selection on getting someone's schedule
                _viewModel.Settings["class"] = _viewModel.SelectedClass;
                _viewModel.Settings["studentId"] = _viewModel.SelectedStudent.StudentId;
                _viewModel.Settings["studentName"] = _viewModel.SelectedStudent.Name;
                _viewModel.Settings.Save();

                query = string.Format("?class={0}&studentId={1}&studentName={2}",
                    _viewModel.SelectedClass,
                    _viewModel.SelectedStudent.StudentId,
                    _viewModel.SelectedStudent.Name);
            }

            var url = string.Format("/Views/SchedulePage.xaml{0}", query);
            NavigationService.Navigate(new Uri(url, UriKind.Relative));
        }

        private void GetSchedule_Click(object sender, RoutedEventArgs e)
        {
            GetSchedule(false);
        }
    }
}