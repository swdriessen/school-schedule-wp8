﻿using SchoolSchedule.Models;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SchoolSchedule.Converters
{
    public class SubjectBackgroundBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var subject = (Subject)value;
            
            var class1 = new ClassSpan(8, 40, 9, 25);
            var class2 = new ClassSpan(9, 25, 10, 10);
            var class3 = new ClassSpan(10, 10, 10, 55);
            var class4 = new ClassSpan(11, 15, 12, 00);
            var class5 = new ClassSpan(12, 00, 12, 45);
            var class6 = new ClassSpan(13, 15, 14, 00);
            var class7 = new ClassSpan(14, 00, 14, 45);
            var class8 = new ClassSpan(14, 45, 15, 30);
            var class9 = new ClassSpan(15, 30, 16, 15);
            
            var activeColorBrush = GetResourceBrush("PhoneAccentBrush"); //system resource

            if (subject.Hour == 1 && class1.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 2 && class2.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 3 && class3.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 4 && class4.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 5 && class5.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 6 && class6.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 7 && class7.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 8 && class8.IsNowActive())
                return activeColorBrush;

            if (subject.Hour == 9 && class9.IsNowActive())
                return activeColorBrush;

            //debug color - pretend that the current hour is 4 (11.15-12.00)
            if (Debugger.IsAttached)
            {
                if (subject.Hour == 4)
                    return activeColorBrush;
            }

            if (subject.Cancelled)
                return GetResourceBrush("CancelledSubjectBackgroundColorBrush");

            //default colorbrush
            return GetResourceBrush("DefaultSubjectBackgroundColorBrush");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private SolidColorBrush GetResourceBrush(string name)
        {
            return (SolidColorBrush)Application.Current.Resources[name];
        }
    }
}